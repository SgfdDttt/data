import sys

def distance(bed1,bed2):
  b1=bed1.split(" ")
  b2=bed2.split(" ")
  assert(len(b1)==len(b2))
  out=0
  for ii in range(len(b1)):
    if b1[ii]!=b2[ii]:
      out+=1
  return out

word_list=list()

print('READ ITEM FILE')
first_line=True
for line in open(sys.argv[1], 'r'): # item file
    if first_line:
        first_line=False
    else:
        word_list.append(line[:-1].split(' ')[-1])

print('word_list')
print(len(word_list))
word_set=set(word_list)
print('word_set')
print(len(word_set))

print('READ EMBEDDINGS FILE')
embeddings_list=list()
for line in open(sys.argv[2], 'r'): # embedding file
    coefs=line[:-1].split(" ")
    new_coefs=list()
    for c in coefs:
        if float(c)>0:
            new_coefs.append("1")
        else:
            new_coefs.append("0")
    assert(len(new_coefs)==len(coefs))
    embeddings_list.append(" ".join(new_coefs))

assert(len(embeddings_list)==len(word_list)) # if not there is a mismatch between item file and embeddings file
print('embeddings_list')
print(len(embeddings_list))
embeddings_set=set(embeddings_list)
print('embeddings_set')
print(len(embeddings_set))

embed2word=dict()
for i in range(len(embeddings_list)):
    embed2word[ embeddings_list[i] ] = word_list[i]

f=open(sys.argv[3], 'w')
'''
purity=list()
print('COMPUTE PURITY')
for ii in range(len(embeddings_list)):
  if ii%1000==0:
    print(ii)
  neighbor_words=set()
  for jj in range(len(embeddings_list)):
    if distance(embeddings_list[ii], embeddings_list[jj]) <= 1:
      neighbor_words.add( embed2word[ embeddings_list[jj] ] )
  f.write(str(len(neighbor_words)) + '\n')
  f.flush()
  purity.append(len(neighbor_words))
'''
print('COMPUTE PAIRWISE DISTANCES')
for ii in range(len(embeddings_list)):
  if ii%1000==0:
    print(ii)
  neighbor_words=set()
  for jj in range(len(embeddings_list)):
    f.write(str(ii) + " " + str(jj) + " " + str(distance(embeddings_list[ii], embeddings_list[jj])) + '\n')
  f.flush()
