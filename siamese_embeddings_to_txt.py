import glob
import numpy

filelist=open('/fhgfs/bootphon/scratch/nholzenb/ABfeatures/list.txt','w')
for thing in glob.glob('/fhgfs/bootphon/scratch/roland/downsampling_project/embeddings/*.npy'):
  print(thing)
  filename=thing.split('/')[-1]
  out = open('/fhgfs/bootphon/scratch/nholzenb/ABfeatures/' + filename[:-3] + 'mfcc', 'w')
  filelist.write('/fhgfs/bootphon/scratch/nholzenb/ABfeatures/' + filename[:-3] + 'mfcc\n')
  data = numpy.load(thing)
  out.write('100\n') # sampling rate
  for frame in data:
    out.write(' '.join([str(c) for c in frame]) + '\n')
