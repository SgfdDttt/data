first_line=True
word_counts=dict()
for line in open('buckeye_all_words.item', 'r'):
  if first_line:
    first_line=False
  else:
    word=line[:-1].split(" ")[-1]
    if word not in word_counts:
      word_counts[word]=0
    word_counts[word]+=1

for word in word_counts:
  print(word + ' ' + str(word_counts[word]))
