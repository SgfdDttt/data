local lapp = require 'pl.lapp'
package.path=package.path .. ';/Users/Nils/Documents/workspace/?.lua' .. ';/home/nholzenb/Documents/workspace/?.lua'
local DataLoader = require 'rnn_autoencoder/audio_data_loader'
local opt = lapp[[
-d,--directory (default "")
-L,--list (default "")
-n,--num_lines (default 100)
-o,--out (default out)
]]

local print_to_file = function(tensor, out_file_name)
  local out_file = io.open(out_file_name, 'w')
  out_file:write(sr .. '\n')
  for time = 1,tensor:size(1) do
    for coef =1,tensor:size(2) do
      out_file:write(tensor[time][coef] .. ' ')
    end -- end for coef
    if time < tensor:size(1) then out_file:write('\n') end
  end -- end for time
  out_file:close()
end

local data_loader = DataLoader:new(opt.directory, opt.list)
all_sizes = {}
all_lengths = {}
data_loader:load_mfcc() -- we expect 'u' 'ba' 'zor' 'merk' in that order
local bound_file = io.open(opt.out .. 'gold_boundaries.txt', 'w')
local file_list = io.open(opt.out .. 'list.txt', 'w')
sr = data_loader.sampling_rates[1]
local inverse_new_line_prob = 2
local probs = {8, 4, 0, 0}
local cumulative = 0
for _,p in pairs(probs) do
  cumulative = cumulative + p
end -- end for p
for i=1,opt.num_lines do
  math.randomseed(os.time() + i); math.random(); math.random()
  local boundaries = {0}
  local out
  local line = true
  while line do
    local rand = math.random(0,cumulative-1)
    local word_i = -1
    for j,p in pairs(probs) do
      rand = rand - p
      if rand<0 then
        word_i = j
        break
      end -- end if rand<0
    end -- end for j,p
    local noised = data_loader.data[word_i]:clone() + torch.Tensor(data_loader.data[word_i]:size()):normal(0,5)
    out = out and torch.cat(out, noised, 1) or noised:clone()
    all_sizes[data_loader.data[word_i]:size(1)] = true
    all_lengths[data_loader.data[word_i]:size(1)/sr] = true
    table.insert(boundaries, boundaries[#boundaries]+data_loader.data[word_i]:size(1)/sr)
    local rand = math.random(0,inverse_new_line_prob-1)
    if rand == 0 then
      print_to_file(out, opt.out .. i .. '.mfcc')
      file_list:write(i .. '.mfcc' .. '\n')
      for _,b in pairs(boundaries) do
        bound_file:write(b .. ' ')
      end
      bound_file:write('\n')
      line = false
    end -- end if rand==0
  end -- end while line
end -- end for i
print('sizes')
for size in pairs(all_sizes) do
print(size)
end -- end for size
print('lengths')
for size in pairs(all_lengths) do
print(size)
end -- end for size
bound_file:close()
file_list:close()
