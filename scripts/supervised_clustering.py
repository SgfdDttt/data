import sys
import math

embeddings = list()
# store the embeddings
for line in open(sys.argv[2], 'r'): # embeddings file
    embeddings.append([float(c) for c in line[:-1].split(' ')])
# group the embeddings by the words they correspond to
clusters = dict()
counter=0
first_line=True
for line in open(sys.argv[1], 'r'):
    if first_line:
        first_line=False
    else:
        word=line[:-1].split(' ')[-1]
        if word not in clusters:
            clusters[word]=list()
        clusters[word].append(embeddings[counter])
        counter+=1

assert(counter==len(embeddings))
#word='y-uw-n-ow'
#word='l-ay-k'
#word='ih-t-s'
#word='jh-ah-s-t'
word='eh-v-r-iy-th-ih-ng'
#word='ah-b-aw-t'
#word='p-iy-p-el'
#word='aa-f'


for bed in clusters[word]:
    print(' '.join([str(c) for c in bed]))


'''
for word in clusters:
'''

'''
x = [0 for i in range(len(embeddings[0]))]
xx = [0 for i in range(len(embeddings[0]))]
N = len(clusters[word])

for bed in clusters[word]:
    #print(' '.join([str(c) for c in bed]))
    for ii in range(len(bed)):
        x[ii]+=bed[ii]
        xx[ii]+=bed[ii]*bed[ii]

mean = [c/N for c in x]
variance = [xx[ii]/N - mean[ii]*mean[ii] for ii in range(len(x))]
#print(' '.join([str(c) for c in variance]))

print(word + '   ' + str(N))
for ii in range(len(x)):
   print(str(mean[ii]) + '   ' + str(variance[ii]))
'''
