import sys
import glob

phones=set()
file2phn=dict()

# make phone dictionary
for thing in glob.glob('/fhgfs/bootphon/data/derived_data/BUCKEYE_revised_bootphon/s*/s*/s*.phones'):
    print(thing)
    not_started_yet=True
    filename=thing.split('/')[-1][:-7]
    for line in open(thing, 'r'):
        if not_started_yet:
           if line[0]=='#':
             not_started_yet=False
           continue
        phone = line[:-1].strip(' ').split(' ')[-1]
        if phone.lower()==phone:
            phones.add(phone)
        if filename not in file2phn:
            file2phn[filename]=list()
        file2phn[filename].append(line[:-1].strip(' '))

print(phones)
print(len(phones))

phn2int=dict()
counter=0
for p in list(phones):
    phn2int[p]=counter
    counter+=1

for filename in file2phn:
    print(filename)
    out_file=open('out/' + filename + '.gphn', 'w')
    out_file.write('100\n')
    start_ind=int(float(file2phn[filename][0].strip(' ').split(' ')[0])*100)
    for ii in range(start_ind): # fill beginning with silence
        vec = [0 for a in range(len(phn2int))]
        out_file.write(' '.join([ str(c) for c in vec ]) + '\n')
    for phni in range(1,len(file2phn[filename])):
        start_ind=int(float(file2phn[filename][phni-1].split(" ")[0])*100)
        stop_ind=int(float(file2phn[filename][phni].split(" ")[0])*100)
        p = file2phn[filename][phni].split(" ")[-1]
        for jj in range(start_ind, stop_ind):
            vec = [0 for a in range(len(phn2int))]
            if p.lower()==p: # otherwise it is silence
                vec[ phn2int[p] ] = 1.0
            out_file.write(' '.join([ str(c) for c in vec ]) + '\n')
