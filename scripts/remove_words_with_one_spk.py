import sys

wrd2spk = dict()

first_line=True
for line in open(sys.argv[1], 'r'):
    if first_line:
        first_line=False
        continue
    wrd=line[:-1].split(" ")[4]
    spk=line[:-1].split(" ")[3]
    if wrd not in wrd2spk:
        wrd2spk[wrd]=set()
    wrd2spk[wrd].add(spk)


# stats about words
max_spk=0
for word in wrd2spk:
    max_spk=max(max_spk, len(wrd2spk[word]))

counts=[0 for i in range(max_spk)]

for word in wrd2spk:
    counts[ len(wrd2spk[word])-1 ]+=1

for ii in range(len(counts)):
    if counts[ii]>0:
        print(str(ii+1) + '   ' + str(counts[ii]))


'''
# now print only the words that have at least 2 spkrs
first_line=True
for line in open(sys.argv[1], 'r'):
    if first_line:
        print(line[:-1])
        first_line=False
        continue
    wrd=line[:-1].split(" ")[4]
    spk=line[:-1].split(" ")[3]
    if len(wrd2spk[wrd])>1:
        print(line[:-1])
'''
