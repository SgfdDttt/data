import sys
import random
words = ['u', 'ba', 'zor', 'merk']
inverse_new_line_prob = 2
probs = [8, 4, 2, 1]
out = open(sys.argv[2], 'w')
cumulative = 0
for p in probs:
  cumulative += p
for i in range(int(sys.argv[1])):
  line = True
  startLine = True
  while line:
    rand = random.randint(0,cumulative-1)
    word_i = -1
    for j,p in enumerate(probs):
      rand -= p
      if rand < 0:
        word_i = j
        break
    if not startLine:
      out.write(' ')
    else:
      startLine = False
    out.write(words[word_i])
    rand = random.randint(0,inverse_new_line_prob-1)
    if rand == 0:
      out.write('\n')
      line = False
out.flush()
out.close()
