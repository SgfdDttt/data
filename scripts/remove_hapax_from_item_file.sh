# remove first line for convenience
cat $1 | awk 'NR > 1'  > word_list
# compute word counts
cat word_list | awk '{w[$5]++}END{for (i in w) print i,w[i]}' > word_counts
# put first line back in
head -1 $1 > $1"_no_hapax"
# read word counts, remove hapaxes
awk '{ if (NR==FNR) {n[$1]=$2} else { if (n[$5]>1) print $0 } }' word_counts $1 >> $1"_no_hapax"
