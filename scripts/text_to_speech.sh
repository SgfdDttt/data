num_lines=`wc -l $1 | awk '{print $1}'`
mkdir -p $1"_speech"
rm $1"_speech/list"
touch $1"_speech/list"
for line in `seq -w 1 $num_lines`; do
  cat $1 | awk -v var_line="$line" 'NR==var_line' | say -o $1"_speech/"$line".wav" --data-format=LEF32@16000
  echo $line".aiff" >> $1"_speech/list"
done
