import sys

lines = list()
for line in open(sys.argv[1], 'r'): # the item file with the syllables
    lines.append(line[:-1]) # store the entire file in a list, for iteration purposes

print('#file onset offset #speaker word')
for pos in range(len(lines)):
    for length in range(3,11): # we look at sequences of 3 to 15 phonemes
        is_valid_sequence=True
        start_file=lines[pos].split(" ")[0]
        for ii in range(length): # check that this sequence contains only phonemes, no silence or other noises; and that no file boundaries are crossed
            if ii+pos==len(lines): # stop if you get to the end of the file
                is_valid_sequence=False
                break
            phone=lines[pos+ii].split(" ")[-1]
            filename=lines[pos+ii].split(" ")[0]
            is_valid_sequence = is_valid_sequence and (phone.lower()==phone) and (start_file==filename)
        if is_valid_sequence:
            start_time=lines[pos].split(" ")[1]
            stop_time=lines[pos+length-1].split(" ")[2]
            speaker=filename[1:3]
            label='-'.join([lines[pos+ii].split(" ")[-1] for ii in range(length)])
            print(start_file + ' ' + start_time + ' ' + stop_time + ' ' + speaker + ' ' + label)
