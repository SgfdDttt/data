local lapp = require 'pl.lapp'
package.path=package.path .. ';/Users/Nils/Documents/workspace/?.lua;/home/nholzenb/Documents/workspace/?.lua'
local DataLoader = require 'rnn_autoencoder/audio_data_loader'
local opt = lapp[[
-d,--directory (default "")
-L,--list (default "")
-n,--num_lines (default 100)
-o,--out (default out)
]]

local data_loader = DataLoader:new(opt.directory, opt.list)
data_loader:load_data() -- we expect 'u' 'ba' 'zor' 'merk' in that order
local sr = data_loader.sampling_rates[1]
local inverse_new_line_prob = 5
local probs = {8, 4, 2, 1}
local cumulative = 0
for _,p in pairs(probs) do
  cumulative = cumulative + p
end -- end for p
for i=1,opt.num_lines do
  math.randomseed(os.time() + i); math.random(); math.random()
  local out
  local line = true
  while line do
    local rand = math.random(0,cumulative-1)
    local word_i = -1
    for j,p in pairs(probs) do
      rand = rand - p
      if rand < 0 then
        word_i = j
        break
      end -- end if
    end -- end for j,p
    out = out and torch.cat(out, data_loader.data[word_i], 1) or data_loader.data[word_i]:clone()
    local rand = math.random(0,inverse_new_line_prob-1)
    if rand == 0 then
      audio.save(opt.out .. i .. '.aiff', out, sr)
      line = false
    end -- end if
  end -- end while line
end -- end for num_lines
-- TODO also write the boundaries to file
