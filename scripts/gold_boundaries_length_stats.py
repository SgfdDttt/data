import sys
lengths=set()
for line in open(sys.argv[1],'r'):
    boundaries = line[:-1].strip(' ').split(' ')
    for ii in range(len(boundaries)-1):
        lengths.add(float(boundaries[ii+1])-float(boundaries[ii]))
lengths = list(lengths)
lengths.sort()
print(lengths)
