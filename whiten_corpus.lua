file_list = arg[1]
input_directory = arg[2] -- directory containing the txt files

file2tensor, file2sr = {},{}
print('load from mfccs')
input_files = {} -- contains the file names to be found in the directory
for line in io.lines(file_list) do
  input_files[line] = true
end -- end for file
print(input_files)
for file in pairs(input_files) do
  print(file)
  local mfcc_file = io.lines(input_directory .. '/' .. file)
  local sr = mfcc_file() -- first line is sampling rate
  local buffer = {} -- each table in buffer is a frame
  for vec in mfcc_file do
    buffer[#buffer+1] = {}
    for coef in vec:gmatch('%S+') do
      table.insert(buffer[#buffer], coef)
    end -- end for coef
  end -- end for vec
  file_key = string.sub(file,1,-6)
  file2sr[file_key] = sr
  file2tensor[file_key] = torch.Tensor(buffer):clone()
end -- end for file
---[[
print('whiten corpus')
total_length = 0
local x = torch.zeros(39) -- or whatever the dimension of mfccs is
local xx = torch.zeros(39)
for key,tens in pairs(file2tensor) do
  total_length = total_length + tens:size(1)
  for jj=1,tens:size(1) do
    x = x + tens[jj]
    xx = xx + torch.pow(tens[jj], 2)
  end
end
local mean = torch.div(x, total_length)
print('mean')
print(mean)
local sigma = torch.div(xx, total_length) - torch.pow(mean, 2)
sigma:sqrt()
print('sigma')
print(sigma)
for key in pairs(file2tensor) do
  for jj=1,file2tensor[key]:size(1) do
    file2tensor[key][jj]:csub(mean):cdiv(sigma)
  end
end
--]]
print('save')
torch.save('whitened_corpus.t7', {file2tensor = file2tensor, file2sr = file2sr})
