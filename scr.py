files = set()

firstLine=True
for line in open('devpart1_file_list.txt', 'r'):
  if firstLine:
    firstLine=False
  else:
    files.add(line[:-1])

firstLine=True
for line in open('buckeye_all_words.item', 'r'):
  if firstLine:
    firstLine=False
  else:
    if line.split(' ')[0] in files:
      print(line[:-1])
