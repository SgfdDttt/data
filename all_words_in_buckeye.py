import glob

print('#file onset offset #speaker word')

for thing in glob.glob('/fhgfs/bootphon/data/derived_data/BUCKEYE_revised_bootphon/*/*/*.words'):
  filename=thing.split('/')[-1].split('.')[0]
  speaker=filename[1:3]
  irrelevant=True
  times=list()
  words=list()
  for line in open(thing, 'r'):
    if irrelevant:
      if line[0]=='#':
        irrelevant=False
    else:
      time=line[:-1].strip(' ').split(' ')[0]
      phones=line[:-1].split(';')[1]
      word="-".join(phones.strip(" ").split(" "))
      times.append(time)
      words.append(word)
  for i in range(1,len(words)):
    if words[i].lower()==words[i]:
      out=filename + " " + times[i-1] + " " + times[i] + " " + speaker + " " + words[i]
      print(out)
